# Changelog

#### v1.0.2

- Feature: add `-ns` flag ('no stripping') to generate a symbol table.

#### v1.0.1

- Fix: do not wait for a key press after exit; causes clm to hang.

## v1.0.0

First tagged version.
