# linker

This is the repository of the [Clean][] static linker for Windows.

This is a delayed mirror of the [upstream][] version and is only used to
publish the package. Periodically changes from upstream are released in a new
version here.

The linker is released in the `base-linker` package, which is included in
`base`.

## Maintainer & license

This project is maintained by [Camil Staps][].
The upstream is maintained by John van Groningen.

For license details see the [LICENSE](/LICENSE) file.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[upstream]: https://svn.cs.ru.nl/repos/clean-dynamics-system
